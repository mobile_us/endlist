package br.com.mobileus.endlist.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinniciusfs on 18/10/16.
 */
public class ListEnd {
    List<EndBean> listEnderecos = new ArrayList<>();

    private static ListEnd ourInstance = new ListEnd();

    public static ListEnd getInstance() {
        return ourInstance;
    }

    private ListEnd() {
        listEnderecos.add(
                new EndBean(
                        "UFSCar",
                        "Rodovia João Leme",
                        "S/N",
                        "Itinga",
                        "Sorocaba",
                        "São Paulo",
                        "Brasil")
        );
    }

    public List<EndBean> getListEnderecos() {
        return listEnderecos;
    }

    public void setListEnderecos(List<EndBean> listEnderecos) {
        this.listEnderecos = listEnderecos;
    }
}
