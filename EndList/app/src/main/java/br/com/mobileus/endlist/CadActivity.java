package br.com.mobileus.endlist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.mobileus.endlist.model.EndBean;
import br.com.mobileus.endlist.model.ListEnd;

public class CadActivity extends AppCompatActivity {
    TextView txtNome;
    TextView txtRua;
    TextView txtNumero;
    TextView txtBairro;
    TextView txtCidade;
    TextView txtEstado;
    TextView txtPais;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cad);

        txtNome = (TextView) findViewById(R.id.txtNome);
        txtRua = (TextView) findViewById(R.id.txtRua);
        txtNumero = (TextView) findViewById(R.id.txtNumero);
        txtBairro = (TextView) findViewById(R.id.txtBairro);
        txtCidade = (TextView) findViewById(R.id.txtCidade);
        txtEstado = (TextView) findViewById(R.id.txtEstado);
        txtPais = (TextView) findViewById(R.id.txtPais);
    }

    public void cadastrar(View view) {
        String nome = txtNome.getText().toString();
        String rua = txtRua.getText().toString();
        String numero = txtNumero.getText().toString();
        String bairro = txtBairro.getText().toString();
        String cidade = txtCidade.getText().toString();
        String estado = txtEstado.getText().toString();
        String pais = txtPais.getText().toString();
        EndBean endereco = new EndBean(nome, rua, numero, bairro, cidade, estado, pais);
        ListEnd.getInstance().getListEnderecos().add(endereco);
        limparCampos();
    }

    private void limparCampos() {
        txtNome.setText("");
        txtRua.setText("");
        txtNumero.setText("");
        txtBairro.setText("");
        txtCidade.setText("");
        txtEstado.setText("");
        txtPais.setText("");
    }

    public void voltar(View view) {
        finish();
    }
}
