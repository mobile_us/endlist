package br.com.mobileus.endlist;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;

public class CadUserActivity extends AppCompatActivity {
    private static final String TAG = "DEBUG_AUTHENTICATION";
    TextView txtEmail;
    TextView txtConfirmarEmail;
    TextView txtSenha;
    TextView txtConfirmarSenha;
    String email;
    String email2;
    String senha;
    String senha2;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cad_user);

        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtConfirmarEmail = (TextView) findViewById(R.id.txtConfirmarEmail);
        txtSenha = (TextView) findViewById(R.id.txtSenha);
        txtConfirmarSenha = (TextView) findViewById(R.id.txtConfirmarSenha);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    public void cadastrar(View view) {
        getUserData();
        if(checkData()){
            cadNewUserFB();
        }
    }

    private void getUserData() {
        email = txtEmail.getText().toString();
        email2 = txtConfirmarEmail.getText().toString();
        senha = txtSenha.getText().toString();
        senha2 = txtConfirmarSenha.getText().toString();
    }

    private boolean checkData(){
        return email.equals(email2) && senha.equals(senha2)? true : false;
    }

    private void cadNewUserFB(){
        mAuth.createUserWithEmailAndPassword(email, senha)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            try {
                                throw task.getException();
                            } catch(FirebaseAuthWeakPasswordException e) {
                                e.printStackTrace();
                                showErrorField(txtSenha, R.string.error_weak_password);
                            } catch(FirebaseAuthInvalidCredentialsException e) {
                                e.printStackTrace();
                                showErrorField(txtEmail, R.string.error_invalid_email);
                            } catch(FirebaseAuthUserCollisionException e) {
                                e.printStackTrace();
                                showErrorField(txtEmail, R.string.error_user_exists);
                            } catch(Exception e) {
                                e.printStackTrace();
                                Toast.makeText(CadUserActivity.this, R.string.error_generic,
                                        Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Intent intent = new Intent(CadUserActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                    }
                });
    }

    private void showErrorField(TextView field, int error_message) {
        field.setError(getString(error_message));
        field.requestFocus();
    }
}
