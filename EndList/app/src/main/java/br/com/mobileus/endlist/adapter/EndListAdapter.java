package br.com.mobileus.endlist.adapter;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.mobileus.endlist.R;
import br.com.mobileus.endlist.model.EndBean;

/**
 * Created by vinniciusfs on 25/10/16.
 */

public class EndListAdapter extends ArrayAdapter<EndBean> implements View.OnClickListener {
    private List<EndBean> endList;
    Context mContext;

    private static class ViewHolder{
        TextView txtNome;
        TextView txtRuaNumBairro;
        TextView txtCidade;
        TextView txtEstadoPais;
    }

    public EndListAdapter(Context context, int resource) {
        super(context, resource);
    }

    public EndListAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public EndListAdapter(Context context, int resource, EndBean[] objects) {
        super(context, resource, objects);
    }

    public EndListAdapter(Context context, int resource, int textViewResourceId, EndBean[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public EndListAdapter(Context context, int resource, List<EndBean> objects) {
        super(context, resource, objects);
    }

    public EndListAdapter(Context context, int resource, int textViewResourceId, List<EndBean> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public EndListAdapter(ArrayList<EndBean> data, Context context){
        super(context, R.layout.item_list, data);
        this.endList = data;
        this.mContext=context;
    }

    @Override
    public void onClick(View view) {
        int position=(Integer) view.getTag();
        Object object= getItem(position);
        EndBean dataModel= (EndBean) object;
        Snackbar.make(view, "Endereço de: " + dataModel.getNome(), Snackbar.LENGTH_LONG).setAction("No action", null).show();
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        EndBean endereco = getItem(position);

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_list, parent, false);

            viewHolder.txtNome = (TextView) convertView.findViewById(R.id.txtNome);
            viewHolder.txtRuaNumBairro = (TextView) convertView.findViewById(R.id.txtRuaNumBairro);
            viewHolder.txtCidade = (TextView) convertView.findViewById(R.id.txtCidade);
            viewHolder.txtEstadoPais = (TextView) convertView.findViewById(R.id.txtEstadoPais);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        lastPosition = position;

        viewHolder.txtNome.setText(endereco.getNome());
        viewHolder.txtRuaNumBairro.setText(endereco.getRua() +", "+ endereco.getNumero() +", "+endereco.getBairro());
        viewHolder.txtCidade.setText(endereco.getCidade());
        viewHolder.txtEstadoPais.setText(endereco.getEstado() +" - "+ endereco.getPais());

        return convertView;
    }
}
