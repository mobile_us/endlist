package br.com.mobileus.endlist;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import br.com.mobileus.endlist.adapter.EndListAdapter;
import br.com.mobileus.endlist.model.EndBean;
import br.com.mobileus.endlist.model.ListEnd;

public class MainActivity extends AppCompatActivity {
    private ListView lstEnd;
    private EndListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lstEnd = (ListView) findViewById(R.id.lstEnd);

        setDataListView();
    }

    private void setDataListView() {
        adapter = new EndListAdapter((ArrayList<EndBean>) ListEnd.getInstance()
                .getListEnderecos(), getApplicationContext());
        lstEnd.setAdapter(adapter);
        lstEnd.setOnItemClickListener(
            new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent,
                                        View view,
                                        int position,
                                        long id) {
                    EndBean dataModel =
                            ListEnd.getInstance().getListEnderecos().get(position);
                    Snackbar.make(
                            view, "Endereço de: " + dataModel.getNome(),
                            Snackbar.LENGTH_LONG).setAction("No action", null).show();
                }
            }
        );
    }

    public void callCad(View view) {
        Intent intent = new Intent(this, CadActivity.class);
        startActivity(intent);
    }
}
